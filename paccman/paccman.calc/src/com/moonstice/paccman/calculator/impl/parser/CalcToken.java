/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

/**
 * Calculator lexical tokens.
 * @author joaof <jsc372@moonstice.com>
 */
public enum CalcToken {

    _0('0'),
    _1('1'),
    _2('2'),
    _3('3'),
    _4('4'),
    _5('5'),
    _6('6'),
    _7('7'),
    _8('8'),
    _9('9'),
    SIGN('s'),
    POINT('.'),
    EVAL('='),
    RESET('z'),
    PLUS('+'),
    MINUS('-'),
    MULT('x'),
    DIV('/'),
    OPEN_PAREN('('),
    CLOSE_PAREN(')'),
    CE('c'),
    PC('%'),
    BACKSPACE('b');

    char c;

    public char getChar() {
        return c;
    }

    boolean isDigit() {
        return c <= '9' && c >= '0';
    }

    boolean isNumberCharacter() {
        return isDigit() || this == SIGN || this == POINT;
    }

    private CalcToken(char c) {
        this.c = c;
    }

    static CalcToken getToken(char in) {
        for (CalcToken token : values()) {
            if (token.c == in) {
                return token;
            }
        }
        throw new IllegalStateException("'" + Character.toString(in)
                + "' is not a valid input character.");
    }

}
