/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Stack;

/**
 * Expression parser.
 * @author joaof <jsc372@moonstice.com>
 */
public class ExprParser {

    static final int ADD_PRIORITY = 1;

    static final int MULT_PRIORITY = 2;

    static final int MAX_PAREN_LEVEL = 128;

    private final int maxPrecision;

    private final MathContext mathContext;

    private final BigDecimal maxNumber;

    int parenLevel;

    private boolean waitOperand;

    private ExprToken previousToken;

    private BigDecimal register;

    void reset() {
        operandStack.clear();
        operatorStack.clear();
        fatalError = null;
        waitOperand = false;
        previousToken = null;
        register = null;
    }

    private boolean evaluateToPriority(int priority) {
        while (!operatorStack.isEmpty() && priority <= operatorStack.peek().getPriority()) {
            if (!evaluateLastOp()) {
                return false;
            }
        }
        return true;
    }

    private boolean forceCloseOpenParen() {
        while (parenLevel > 0) {
            if (!parseCloseParen()) {
                return false;
            }
        }
        return true;
    }

    private int getParenPriorityAdjust() {
        return parenLevel * 10;
    }

    private Operator getOperator(CalcToken token) throws IllegalStateException {
        Operator op;
        switch (token) {
            case PLUS:
                op = new AddOperator(parenLevel);
                break;

            case MINUS:
                op = new SubtractOperator(parenLevel);
                break;

            case MULT:
                op = new MultOperator(parenLevel);
                break;

            case DIV:
                op = new DivOperator(parenLevel);
                break;

            default:
                throw new IllegalStateException("Invalid operator");
        }
        return op;
    }

    private void injectOperandIfMissing() {
        if (waitOperand) {
            pushOperand(register);
            waitOperand = false;
        }
    }

    private boolean isEvalToken(ExprToken token) {
        return token != null && token.isEvalToken();
    }

    public enum FatalError {

        OVERFLOW,
        DIV_BY_ZERO

    };

    FatalError fatalError;

    void setFatalError(FatalError fatalError) {
        this.fatalError = fatalError;
    }

    public FatalError getFatalError() {
        return fatalError;
    }

    boolean inError() {
        return fatalError != null;
    }

    Stack<BigDecimal> operandStack = new Stack<>();

    Stack<Operator> operatorStack = new Stack<>();

    BigDecimal getRegister() {
        if (register == null) {
            return BigDecimal.ZERO;
        } else {
            return register;
        }
    }

    BigDecimal round(BigDecimal number, int precision) {
        BigInteger intPart = number.abs().toBigInteger();
        BigDecimal decPart = number.abs().subtract(new BigDecimal(intPart)).stripTrailingZeros();

        BigDecimal result;
        int maxDecPart = Math.max(0, precision - intPart.toString().length());
        if (maxDecPart > 0) {
            maxDecPart--;
        }

        decPart = decPart.setScale(maxDecPart, RoundingMode.HALF_DOWN).stripTrailingZeros();

        if (decPart.compareTo(BigDecimal.ZERO) == 0) {
            result = new BigDecimal(intPart);
        } else {
            result = new BigDecimal(intPart).add(decPart);
        }

        if (result.compareTo(maxNumber) > 0) {
            setFatalError(FatalError.OVERFLOW);
            return null;
        }

        return number.signum() < 0 ? result.negate() : result;

    }

    abstract class Operator {

        private final int parenLevelWhenParsed;

        private final int basePriority;

        public Operator(int basePriority, int parenLevelWhenParsed) {
            this.basePriority = basePriority;
            this.parenLevelWhenParsed = parenLevelWhenParsed;
        }

        public int getPriority() {
            return parenLevelWhenParsed * 10 + basePriority;
        }

        BigDecimal doEvaluate(BigDecimal firstOperand, BigDecimal secondOperand) {
            BigDecimal result = evaluate(firstOperand, secondOperand);
            return (result == null) ? null : round(result, maxPrecision);
        }

        protected abstract BigDecimal evaluate(BigDecimal firstOperand, BigDecimal secondOperand);

    }

    class AddOperator extends Operator {

        public AddOperator(int parenLevelWhenParsed) {
            super(ADD_PRIORITY, parenLevelWhenParsed);
        }

        @Override
        protected BigDecimal evaluate(BigDecimal firstOperand, BigDecimal secondOperand) {
            return firstOperand.add(secondOperand);
        }

    }

    class MultOperator extends Operator {

        public MultOperator(int parenLevelWhenParsed) {
            super(MULT_PRIORITY, parenLevelWhenParsed);
        }

        @Override
        protected BigDecimal evaluate(BigDecimal firstOperand, BigDecimal secondOperand) {
            return firstOperand.multiply(secondOperand);
        }

    }

    class DivOperator extends Operator {

        public DivOperator(int parenLevelWhenParsed) {
            super(MULT_PRIORITY, parenLevelWhenParsed);
        }

        @Override
        protected BigDecimal evaluate(BigDecimal firstOperand, BigDecimal secondOperand) {
            if (secondOperand.compareTo(BigDecimal.ZERO) == 0) {
                setFatalError(FatalError.DIV_BY_ZERO);
                return null;
            }
            return firstOperand.divide(secondOperand, mathContext);
        }

    }

    class SubtractOperator extends Operator {

        public SubtractOperator(int parenLevelWhenParsed) {
            super(ADD_PRIORITY, parenLevelWhenParsed);
        }

        @Override
        protected BigDecimal evaluate(BigDecimal firstOperand, BigDecimal secondOperand) {
            return firstOperand.subtract(secondOperand);
        }

    }

    public boolean evaluateLastOp() {
        Operator op = operatorStack.pop();

        BigDecimal secondOperand = operandStack.pop();
        BigDecimal firstOperand = operandStack.pop();
        final BigDecimal result = op.doEvaluate(firstOperand, secondOperand);
        if (result == null) {
            return false;
        } else {
            pushOperand(result);
            return true;
        }
    }

    ExprParser(int maxPrecision) {
        this.maxPrecision = maxPrecision;
        this.mathContext = new MathContext(2 * maxPrecision, RoundingMode.HALF_DOWN);
        StringBuilder sb = new StringBuilder(maxPrecision);
        for (int i = 0; i < maxPrecision; i++) {
            sb.append('9');
        }
        this.maxNumber = new BigDecimal(sb.toString());
    }

    private boolean parseNumber(BigDecimal bd) {
        if (isEvalToken(previousToken) && !operandStack.empty()) {
            operandStack.pop();
        }
        pushOperand(round(bd, maxPrecision));
        waitOperand = false;
        return true;
    }

    private boolean parseEval() {
        injectOperandIfMissing();
        if (!forceCloseOpenParen()) {
            return false;
        }

        boolean res;
        if (operatorStack.isEmpty()) {
            res = true;
        } else {
            res = evaluateToPriority(-1);

            assert inError() || !(operandStack.size() > 1) : "operandStack.size() is " + Integer.toString(operandStack.size())
                    + " (it should be 1 or 0)";
            assert inError() || operatorStack.isEmpty();
            assert inError() || parenLevel == 0;
        }
        return res;
    }

    private boolean parseOperator(CalcToken token) {
        if (operandStack.isEmpty()) {
            pushOperand(BigDecimal.ZERO);
        }
        injectOperandIfMissing();
        waitOperand = true;
        final Operator op = getOperator(token);
        if (!evaluateToPriority(op.getPriority())) {
            return false;
        }
        operatorStack.push(op);
        return true;
    }

    private boolean parseOpenParen() {
        if (isEvalToken(previousToken) && !operandStack.empty()) {
            register = null;
            operandStack.pop();
        }
        if (parenLevel == MAX_PAREN_LEVEL) {
            return false;
        }
        parenLevel++;
        return true;
    }

    private boolean parseCloseParen() {
        assert parenLevel > 0;

        injectOperandIfMissing();
        if (!evaluateToPriority(getParenPriorityAdjust())) {
            return false;
        }
        parenLevel--;

        return true;
    }

    private boolean parsePc() {
        if (operatorStack.empty() || operatorStack.peek().parenLevelWhenParsed < parenLevel) {
            return false;
        } else {
            injectOperandIfMissing();
            BigDecimal pc = operandStack.pop();
            BigDecimal val = operandStack.peek();
            val = round(val.multiply(pc).divide(new BigDecimal("100")), maxPrecision);
            if (val == null) {
                return false;
            } else {
                pushOperand(val);
                return true;
            }
        }
    }

    boolean parseToken(ExprToken token, Object... args) {
        assert getFatalError() == null;

        boolean result;

        switch (token) {
            case EVAL:
                result = parseEval();
                break;

            case CLOSE_PAREN:
                result = parseCloseParen();
                break;

            case OPEN_PAREN:
                result = parseOpenParen();
                break;

            case OPERATOR:
                result = parseOperator((CalcToken) args[0]);
                break;

            case NUMBER:
                result = parseNumber((BigDecimal) args[0]);
                break;

            case PC:
                result = parsePc();
                break;

            default:
                throw new UnsupportedOperationException("Not yet implemented");
        }
        previousToken = token;
        return result;
    }

    void pushOperand(BigDecimal operand) {
        operandStack.push(operand);
        register = operand;
    }

}
