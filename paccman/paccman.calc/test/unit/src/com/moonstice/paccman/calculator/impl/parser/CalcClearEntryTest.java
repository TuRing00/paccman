/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Class test for CE key.
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcClearEntryTest extends CalcParserTestBase {

    public CalcClearEntryTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testCeOnParsingNumber() {
        System.out.println("CeOnParsingNumber");

        testStringParse("c123=", "0", "1", "12", "123", "123");
        testStringParse("12c=", "1", "12", "0", "0");
        testStringParse("12.3c=", "1", "12", "12.", "12.3", "0", "0");
        testStringParse("1s2.3c=", "1", "-1", "-12", "-12.", "-12.3", "0", "0");
    }

    @Test
    public void testNoEffectCe() {
        System.out.println("NoEffectCe");

        testStringParse("c=", "0", "0");
        testStringParse("0c=", "0", "0", "0");
        testStringParse("=c=", "0", "0", "0");

        testStringParse("cc=", "0", "0", "0");
        testStringParse("c0c=", "0", "0", "0", "0");
        testStringParse("c=c=", "0", "0", "0", "0");

        testStringParse("0cc=", "0", "0", "0", "0");
        testStringParse("=cc=", "0", "0", "0", "0");

        testStringParse("c=c", "0", "0", "0");
        testStringParse("0cc=", "0", "0", "0", "0");
        testStringParse("=c=c", "0", "0", "0", "0");
    }

    @Test
    public void testParseNumberAfterCe() {
        System.out.println("ParseNumberAfterCe");

        testStringParse("12c456=", "1", "12", "0", "4", "45", "456", "456");
        testStringParse("1s2c456=", "1", "-1", "-12", "0", "4", "45", "456", "456");
        testStringParse("12.98c456=", "1", "12", "12.", "12.9", "12.98", "0", "4", "45", "456", "456");
        testStringParse("1s2.98c456=", "1", "-1", "-12", "-12.", "-12.9", "-12.98", "0", "4", "45", "456", "456");

        testStringParse("12c45s6=", "1", "12", "0", "4", "45", "-45", "-456", "-456");
        testStringParse("1s2c4s56=", "1", "-1", "-12", "0", "4", "-4", "-45", "-456", "-456");
        testStringParse("12.98c456s=", "1", "12", "12.", "12.9", "12.98", "0", "4", "45", "456", "-456", "-456");
        testStringParse("1s2.98c45s6=", "1", "-1", "-12", "-12.", "-12.9", "-12.98", "0", "4", "45", "-45", "-456", "-456");

        testStringParse("12c4.56=", "1", "12", "0", "4", "4.", "4.5", "4.56", "4.56");
        testStringParse("1s2c4.56=", "1", "-1", "-12", "0", "4", "4.", "4.5", "4.56", "4.56");
        testStringParse("12.98c4.56=", "1", "12", "12.", "12.9", "12.98", "0", "4", "4.", "4.5", "4.56", "4.56");
        testStringParse("1s2.98c4.56=", "1", "-1", "-12", "-12.", "-12.9", "-12.98", "0", "4", "4.", "4.5", "4.56", "4.56");

        testStringParse("12c4.5s6=", "1", "12", "0", "4", "4.", "4.5", "-4.5", "-4.56", "-4.56");
        testStringParse("1s2c4.s56=", "1", "-1", "-12", "0", "4", "4.", "-4.", "-4.5", "-4.56", "-4.56");
        testStringParse("12.98c4.56s=", "1", "12", "12.", "12.9", "12.98", "0", "4", "4.", "4.5", "4.56", "-4.56", "-4.56");
        testStringParse("1s2.98c4.5s6=", "1", "-1", "-12", "-12.", "-12.9", "-12.98", "0", "4", "4.", "4.5", "-4.5", "-4.56", "-4.56");
    }

    @Test
    public void testCeFirstOperand() {
        System.out.println("CeFirstOperand");

        testStringParse("c+12=", "0", "0", "1", "12", "12");
        testStringParse("c12+=", "0", "1", "12", "12", "24");
        testStringParse("12c+=", "1", "12", "0", "0", "0");
        testStringParse("12c+34=", "1", "12", "0", "0", "3", "34", "34");
        testStringParse("12c78+34=", "1", "12", "0", "7", "78", "78", "3", "34", "112");
    }

    @Test
    public void testCeSecondOperand() {
        System.out.println("CeSecondOperand");

        testStringParse("34+c=", "3", "34", "34", "0", "34");
        testStringParse("+c12=", "0", "0", "1", "12", "12");
        testStringParse("+12c=", "0", "1", "12", "0", "0");
        testStringParse("+1c2=", "0", "1", "0", "2", "2");
        testStringParse("34+12c=", "3", "34", "34", "1", "12", "0", "34");
        testStringParse("34+12c78=", "3", "34", "34", "1", "12", "0", "7", "78", "112");
    }

    @Test
    public void testCeAfterEval() {
        System.out.println("CeAfterEval");

        testStringParse("12=c45+67=", "1", "12", "12", "0", "4", "45", "45", "6",
                "67", "112");
        testStringParse("12=4c5+67=", "1", "12", "12", "4", "0", "5", "5", "6",
                "67", "72");
        testStringParse("12=45+c67=", "1", "12", "12", "4", "45", "45", "0", "6",
                "67", "112");
        testStringParse("12=45+6c7=", "1", "12", "12", "4", "45", "45", "6", "0",
                "7", "52");
        testStringParse("12=c+67=", "1", "12", "12", "0", "0", "6", "67", "67");
        testStringParse("12=4c+67=", "1", "12", "12", "4", "0", "0", "6", "67",
                "67");
        testStringParse("12=45+c=", "1", "12", "12", "4", "45", "45", "0", "45");
        testStringParse("12=45+6c=", "1", "12", "12", "4", "45", "45", "6", "0",
                "45");
        testStringParse("12=45+67c=", "1", "12", "12", "4", "45", "45", "6", "67",
                "0", "45");
    }

    @Test
    public void testCeAroundParen() {
        System.out.println("CeAroundParen");

        testStringParse("12x(34+56)=", "1", "12", "12", "12", "3", "34", "34", "5", "56",
                "90", "1080");
        testStringParse("c12x(34+56)=", "0", "1", "12", "12", "12", "3", "34", "34", "5", "56",
                "90", "1080");
        testStringParse("1c2x(34+56)=", "1", "0", "2", "2", "2", "3", "34", "34", "5", "56",
                "90", "180");
        testStringParse("12cx(34+56)=", "1", "12", "0", "0", "0", "3", "34", "34", "5", "56",
                "90", "0");
        testStringParse("12xc(34+56)=", "1", "12", "12", "0", "0", "3", "34", "34", "5", "56",
                "90", "1080");

        testStringParse("12x(c34+56)=", "1", "12", "12", "12", "0", "3", "34", "34", "5", "56",
                "90", "1080");
        testStringParse("12x(3c4+56)=", "1", "12", "12", "12", "3", "0", "4", "4", "5", "56",
                "60", "720");
        testStringParse("12x(34c+56)=", "1", "12", "12", "12", "3", "34", "0", "0", "5", "56",
                "56", "672");
        testStringParse("12x(34+c56)=", "1", "12", "12", "12", "3", "34", "34", "0", "5", "56",
                "90", "1080");
        testStringParse("12x(34+5c6)=", "1", "12", "12", "12", "3", "34", "34", "5", "0", "6",
                "40", "480");
        testStringParse("12x(34+56c)=", "1", "12", "12", "12", "3", "34", "34", "5", "56", "0",
                "34", "408");
        testStringParse("12x(34+56)c=", "1", "12", "12", "12", "3", "34", "34", "5", "56", "90",
                "0", "0");
        testStringParse("12x(34+56)=c", "1", "12", "12", "12", "3", "34", "34", "5", "56", "90",
                "1080", "0");

    }

}
