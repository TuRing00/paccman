/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Class for testing EVAL key.
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcEvalTest extends CalcParserTestBase {

    public CalcEvalTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testEvalNoOp() {
        System.out.println("EvalNoOp");

        testStringParse("12=3456=", "1", "12", "12", "3", "34", "345", "3456", "3456");
        testStringParse("12=3.456=", "1", "12", "12", "3", "3.", "3.4", "3.45", "3.456", "3.456");
        testStringParse("12=3.4s56=", "1", "12", "12", "3", "3.", "3.4", "-3.4", "-3.45", "-3.456", "-3.456");

        testStringParse("1.2=3456=", "1", "1.", "1.2", "1.2", "3", "34", "345", "3456", "3456");
        testStringParse("1.2=3.456=", "1", "1.", "1.2", "1.2", "3", "3.", "3.4", "3.45", "3.456", "3.456");
        testStringParse("1.2=3.4s56=", "1", "1.", "1.2", "1.2", "3", "3.", "3.4", "-3.4", "-3.45", "-3.456", "-3.456");

        testStringParse("1.2s9=3456=", "1", "1.", "1.2", "-1.2", "-1.29", "-1.29", "3", "34", "345", "3456", "3456");
        testStringParse("1.2s9=3.456=", "1", "1.", "1.2", "-1.2", "-1.29", "-1.29", "3", "3.", "3.4", "3.45", "3.456", "3.456");
        testStringParse("1.2s9=3.4s56=", "1", "1.", "1.2", "-1.2", "-1.29", "-1.29", "3", "3.", "3.4", "-3.4", "-3.45", "-3.456", "-3.456");

        testStringParse("1=2=3=", "1", "1", "2", "2", "3", "3");
    }

    @Test
    public void testEvalNumberAfterEvalOp() {
        System.out.println("EvalNumberAfterEvalOp");

        testStringParse("1+2=5=", "1", "1", "2", "3", "5", "5");
        testStringParse("1+2+4=5=", "1", "1", "2", "3", "4", "7", "5", "5");
    }

    @Test
    public void testEvalOpAfterEvalNumber() {
        System.out.println("EvalOpAfterEvalNumber");

        testStringParse("1=2+5=", "1", "1", "2", "2", "5", "7");
        testStringParse("1=2+5+8=", "1", "1", "2", "2", "5", "7", "8", "15");
    }

}
