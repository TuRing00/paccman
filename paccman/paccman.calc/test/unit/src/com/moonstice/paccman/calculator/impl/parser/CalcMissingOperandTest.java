/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Class for testing when operand iis not enter by the user. In this case, the
 * currently displayed value is used as operand.
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcMissingOperandTest extends CalcParserTestBase {

    public CalcMissingOperandTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testAddWithMissingOperand() {
        System.out.println("AddWithMissingOperand");

        testStringParse("+=", "0", "0");
        testStringParse("+12=", "0", "1", "12", "12");
        testStringParse("1+=", "1", "1", "2");

        testStringParse("12+3+=", "1", "12", "12", "3", "15", "30");
        testStringParse("+1+2=", "0", "1", "1", "2", "3");
        testStringParse("+2+=", "0", "2", "2", "4");
        testStringParse("+12+3=", "0", "1", "12", "12", "3", "15");
        testStringParse("3+5+=", "3", "3", "5", "8", "16");

        testStringParse("3++=", "3", "3", "6", "12");
    }

    @Test
    public void testDivWithMissingOperand() {
        System.out.println("DivWithMissingOperand");

        testStringParse("12/=", "1", "12", "12", "1");
        testStringParse("/1=", "0", "1", "0");
        testStringParse("/=", "0", ExprParser.FatalError.DIV_BY_ZERO);
        testStringParse("/12=", "0", "1", "12", "0");
        testStringParse("2/=", "2", "2", "1");

        testStringParse("12/3/=", "1", "12", "12", "3", "4", "1");
        testStringParse("/1/2=", "0", "1", "0", "2", "0");
        testStringParse("/2/=", "0", "2", "0", ExprParser.FatalError.DIV_BY_ZERO);
        testStringParse("/12/3=", "0", "1", "12", "0", "3", "0");
        testStringParse("3/5/=", "3", "3", "5", "0.6", "1");
    }

    @Test
    public void testMultWithMissingOperand() {
        System.out.println("MultWithMissingOperand");

        testStringParse("12x=", "1", "12", "12", "144");
        testStringParse("x1=", "0", "1", "0");
        testStringParse("x=", "0", "0");
        testStringParse("x12=", "0", "1", "12", "0");
        testStringParse("2x=", "2", "2", "4");

        testStringParse("12x3x=", "1", "12", "12", "3", "36", "1296");
        testStringParse("x1x2=", "0", "1", "0", "2", "0");
        testStringParse("x2x=", "0", "2", "0", "0");
        testStringParse("x12x3=", "0", "1", "12", "0", "3", "0");
        testStringParse("3x5x=", "3", "3", "5", "15", "225");
    }

    @Test
    public void testSubtractWithMissingOperand() {
        System.out.println("SubtractWithMissingOperand");

        testStringParse("12-=", "1", "12", "12", "0");
        testStringParse("-1=", "0", "1", "-1");
        testStringParse("-=", "0", "0");
        testStringParse("-12=", "0", "1", "12", "-12");
        testStringParse("1-=", "1", "1", "0");

        testStringParse("12-3-=", "1", "12", "12", "3", "9", "0");
        testStringParse("-1-2=", "0", "1", "-1", "2", "-3");
        testStringParse("-2-=", "0", "2", "-2", "0");
        testStringParse("-12-3=", "0", "1", "12", "-12", "3", "-15");
        testStringParse("3-5-=", "3", "3", "5", "-2", "0");
    }

    @Test
    public void testMissingOperandInParen() {
        System.out.println("MissingOperandInParen");

        // 5 x ( 3 + {3} )
        testStringParse("5x(3+)=", "5", "5", "5", "3", "3", "6", "30");
        // 5 x ( 3 + 2 x {2} )
        testStringParse("5x(3+2x)=", "5", "5", "5", "3", "3", "2", "2", "7", "35");

        // 5 x ( {5} + 3 )
        testStringParse("5x(+3)=", "5", "5", "5", "5", "3", "8", "40");
        // 5 x ( 3 + {3} x 2 )
        testStringParse("5x(3+x2)=", "5", "5", "5", "3", "3", "3", "2", "9", "45");

    }

    @Test
    public void testMultiOperatorNoOperand() {
        System.out.println("MultiOperatorNoOperand");

        // 3 + {3} x 5 - [18} + 2
        testStringParse("3+x5-+2=", "3", "3", "3", "5", "18", "0", "2", "2");
    }

    @Test
    public void testMissingOperandAfterEval() {
        System.out.println("MissingOperandAfterEval");

        testStringParse("1+2=+5=", "1", "1", "2", "3", "3", "5", "8");
        testStringParse("1+2=x5+2=", "1", "1", "2", "3", "3", "5", "15", "2", "17");
        testStringParse("1+2=+5x2=", "1", "1", "2", "3", "3", "5", "5", "2", "13");
    }

}
