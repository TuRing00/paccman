/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test evaluation of multiple operations.
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcMultipleOpTest extends CalcParserTestBase {

    public CalcMultipleOpTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testTwoAdditions() {
        System.out.println("TwoAdditions");

        testStringParse("1+2=5+6=", "1", "1", "2", "3", "5", "5", "6", "11");
    }

    @Test
    public void testTwoAddsAndOneMultiply() {
        System.out.println("TwoAddsAndOneMultiply");

        testStringParse("1+2=5+6=7x8=", "1", "1", "2", "3", "5", "5", "6", "11",
                "7", "7", "8", "56");
    }

    @Test
    public void testUseEvalResultAsOperand() {
        System.out.println("UseEvalResultAsOperand");

        testStringParse("1+2=+6=", "1", "1", "2", "3", "3", "6", "9");
        testStringParse("1+2=x.6=", "1", "1", "2", "3", "3", "0.", "0.6", "1.8");
        testStringParse("1+2=x0.6=", "1", "1", "2", "3", "3", "0", "0.", "0.6", "1.8");
        testStringParse("1+2=x0.6.=", "1", "1", "2", "3", "3", "0", "0.", "0.6", false, "1.8");
    }

}
