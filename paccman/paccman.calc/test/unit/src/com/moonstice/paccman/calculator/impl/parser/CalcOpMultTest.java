/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Class for testing multiply operation.
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcOpMultTest extends CalcParserTestBase {

    public CalcOpMultTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testMultTwoOperands() {
        System.out.println("MultTwoOperands");
        
        testStringParse("5x2=", "5", "5", "2", "10");
        testStringParse("12x3=", "1", "12", "12", "3", "36");
        testStringParse("6x34=", "6", "6", "3", "34", "204");
        testStringParse("12x45=", "1", "12", "12", "4", "45", "540");
        testStringParse("1.2x3s4=", "1", "1.", "1.2", "1.2", "3", "-3", "-34", "-40.8");
        testStringParse("2x3.4=", "2", "2", "3", "3.", "3.4", "6.8");
        testStringParse("1.2x3.4=", "1", "1.", "1.2", "1.2", "3", "3.", "3.4", "4.08");
    }

    @Test
    public void testMultThreeOperands() {
        System.out.println("MultThreeOperands");
        
        testStringParse("2x3x7=", "2", "2", "3", "6", "7", "42");
        testStringParse("5x23x5.7=", "5", "5", "2", "23", "115", "5", "5.", "5.7", "655.5");
        testStringParse("4x2s3x5.7=", "4", "4", "2", "-2", "-23", "-92", "5", "5.", "5.7", "-524.4");
        testStringParse("4x23x5.7s=", "4", "4", "2", "23", "92", "5", "5.", "5.7", "-5.7", "-524.4");
        testStringParse("4x023x5.7s=", "4", "4", "0", "2", "23", "92", "5", "5.", "5.7", "-5.7", "-524.4");
        testStringParse("4x023x5.70=", "4", "4", "0", "2", "23", "92", "5", "5.", "5.7", "5.70", "524.4");
    }

}
