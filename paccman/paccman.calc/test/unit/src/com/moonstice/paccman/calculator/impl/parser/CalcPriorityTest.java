/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Class for testing priority of operators.
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcPriorityTest extends CalcParserTestBase {

    public CalcPriorityTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    /**
     * Test add / add
     */
    @Test
    public void testPriority_AddVsAdd() {
        System.out.println("Priority_AddVsAdd");

        testStringParse("5+2+3=", "5", "5", "2", "7", "3", "10");
    }

    /**
     * Test add / sub
     */
    @Test
    public void testPriority_AddVsSub() {
        System.out.println("Priority_AddVsSub");

        testStringParse("5+2-3=", "5", "5", "2", "7", "3", "4");
        testStringParse("5-2+4=", "5", "5", "2", "3", "4", "7");
    }

    /**
     * Test add / mult
     */
    @Test
    public void testPriority_AddVsMult() {
        System.out.println("Priority_AddVsMult");

        testStringParse("5x2+3=", "5", "5", "2", "10", "3", "13");
        testStringParse("3+5x2=", "3", "3", "5", "5", "2", "13");
        testStringParse("3+5x2+7x8=", "3", "3", "5", "5", "2", "13", "7", "7",
                "8", "69");
    }

    /**
     * Test add /div
     */
    @Test
    public void testPriority_AddVsDiv() {
        System.out.println("Priority_AddVsDiv");

        testStringParse("5/2+4=", "5", "5", "2", "2.5", "4", "6.5");
        testStringParse("4+5/2=", "4", "4", "5", "5", "2", "6.5");
        testStringParse("3+5/2+7/8=", "3", "3", "5", "5", "2", "5.5", "7", "7",
                "8", "6.375");
    }

    /**
     * Test sub / sub
     */
    @Test
    public void testPriority_SubVsSub() {
        System.out.println("Priority_SubVsSub");

        testStringParse("7-2-3=", "7", "7", "2", "5", "3", "2");
    }

    /**
     * Test sub / mult
     */
    @Test
    public void testPriority_SubVsMult() {
        System.out.println("Priority_SubVsMult");

        testStringParse("5x2-3=", "5", "5", "2", "10", "3", "7");
        testStringParse("3-5x2=", "3", "3", "5", "5", "2", "-7");
        testStringParse("3-5x2-7x8=", "3", "3", "5", "5", "2", "-7", "7", "7",
                "8", "-63");
    }

    /**
     * Test sub / div
     */
    @Test
    public void testPriority_SubVsDiv() {
        System.out.println("Priority_SubVsDiv");

        testStringParse("5/2-4=", "5", "5", "2", "2.5", "4", "-1.5");
        testStringParse("4-5/2=", "4", "4", "5", "5", "2", "1.5");
        testStringParse("3-5/2-7/8=", "3", "3", "5", "5", "2", "0.5", "7", "7",
                "8", "-0.375");
    }

    /**
     * Test mult / mult
     */
    @Test
    public void testPriority_MultVsMult() {
        System.out.println("Priority_MultVsMult");

        testStringParse("7x2x3=", "7", "7", "2", "14", "3", "42");
    }

    /**
     * Test mult / div
     */
    @Test
    public void testPriority_MultVsDiv() {
        System.out.println("Priority_MultVsDiv");

        testStringParse("5x2/4=", "5", "5", "2", "10", "4", "2.5");
        testStringParse("5/2x4=", "5", "5", "2", "2.5", "4", "10");
    }

    /**
     * Test div / div
     */
    @Test
    public void testPriority_DivVsDiv() {
        System.out.println("Priority_DivVsDiv");

        testStringParse("7/2/4=", "7", "7", "2", "3.5", "4", "0.875");
    }

}
