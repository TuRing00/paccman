--
-- Generated on 2014-12-05 00:09:26.425888 from MySQL Workbench using mysql2derby_plugin, version 10.
--

--
-- Tables creation
--

CREATE TABLE ObjAccountTypes(
    idAccountType INTEGER NOT NULL,
    name VARCHAR(128) NOT NULL,
    PRIMARY KEY(idAccountType)
);

CREATE TABLE ObjAddresses(
    idAddress INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
    number VARCHAR(16),
    addressLine1 VARCHAR(128),
    addressLine2 VARCHAR(128),
    zip VARCHAR(16),
    city VARCHAR(128),
    idCountry CHAR(2),
    PRIMARY KEY(idAddress)
);

CREATE TABLE RefCountries(
    codeISO3166 CHAR(2) NOT NULL,
    englishName VARCHAR(128),
    PRIMARY KEY(codeISO3166)
);

CREATE TABLE ObjInfos(
    idInfo INTEGER NOT NULL,
    phone1 VARCHAR(128),
    phone2 VARCHAR(128),
    fax VARCHAR(128),
    email VARCHAR(128),
    web VARCHAR(128),
    PRIMARY KEY(idInfo)
);

CREATE TABLE ObjPersons(
    idPerson INTEGER NOT NULL,
    idAddress INTEGER,
    idInfo INTEGER,
    firstName VARCHAR(128),
    lastName VARCHAR(128),
    PRIMARY KEY(idPerson)
);

CREATE TABLE ObjBanks(
    idBank INTEGER NOT NULL,
    bankName VARCHAR(128) NOT NULL,
    agency VARCHAR(128),
    idBankInfo INTEGER,
    idBankAddress INTEGER,
    idBanker INTEGER,
    PRIMARY KEY(idBank)
);

CREATE TABLE ObjAccounts(
    idAccount INTEGER NOT NULL,
    name VARCHAR(128),
    accountNumber VARCHAR(128),
    accountNumberKey VARCHAR(16),
    idAccountHolder INTEGER,
    idBank INTEGER NOT NULL,
    initialBalance DECIMAL(20,5) NOT NULL DEFAULT 0.0,
    lastReconciliationDate DATE,
    lastReconsiliationDate DATE,
    reconciliationStatus VARCHAR(255) NOT NULL DEFAULT 'U',
    idAccountType INTEGER NOT NULL,
    PRIMARY KEY(idAccount)
);

CREATE TABLE ObjTransactions(
    idTransaction INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
    idAccount INTEGER NOT NULL,
    idPaiementMode INTEGER,
    label VARCHAR(128),
    reconciliationStatus VARCHAR(255) NOT NULL DEFAULT 'U',
    reconciliationDate DATE,
    transactionDate DATE NOT NULL,
    valueDate DATE NOT NULL,
    idPayee INTEGER,
    PRIMARY KEY(idTransaction)
);

CREATE TABLE ObjPaiementModes(
    idPaiementMode INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
    paiementModeName VARCHAR(128) NOT NULL,
    PRIMARY KEY(idPaiementMode)
);

CREATE TABLE ObjTransactionEntries(
    idTransactionEntry INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
    idCategory INTEGER NOT NULL,
    amount DECIMAL(20,5) NOT NULL,
    note VARCHAR(1024),
    idTransaction INTEGER NOT NULL,
    PRIMARY KEY(idTransactionEntry)
);

CREATE TABLE ObjCategories(
    idCategory INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
    categoryName VARCHAR(128) NOT NULL,
    idCategoryGroup INTEGER NOT NULL,
    description VARCHAR(1024),
    isIncome BOOLEAN NOT NULL,
    PRIMARY KEY(idCategory)
);

CREATE TABLE ObjCategoryGroups(
    idCategoryGroup INTEGER NOT NULL,
    categoryGroupName VARCHAR(128) NOT NULL,
    description VARCHAR(1024),
    PRIMARY KEY(idCategoryGroup)
);

CREATE TABLE ObjTransfers(
    idTransactionEntry INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
    idOtherTransactionEntry INTEGER NOT NULL,
    PRIMARY KEY(idTransactionEntry)
);

CREATE TABLE ObjTransactionModels(
    idTransactionModel INTEGER NOT NULL,
    transactionModelName VARCHAR(128) NOT NULL,
    description VARCHAR(1024),
    PRIMARY KEY(idTransactionModel)
);

CREATE TABLE AssoCategoriesTransactionModels(
    idCategory INTEGER NOT NULL,
    idTransactionModel INTEGER NOT NULL,
    PRIMARY KEY(idCategory, idTransactionModel)
);

CREATE TABLE ObjScheduledTransactions(
    idTransactionId INTEGER NOT NULL,
    shortDescription VARCHAR(256) NOT NULL,
    nextOccurrence DATE NOT NULL,
    period INTEGER NOT NULL,
    periodUnit VARCHAR(255) NOT NULL,
    daysBeforeNotification INTEGER NOT NULL DEFAULT 0,
    isAutomatic BOOLEAN NOT NULL DEFAULT TRUE,
    isFixedAmount BOOLEAN NOT NULL DEFAULT TRUE,
    PRIMARY KEY(idTransactionId)
);

CREATE TABLE ObjPayees(
    idPayee INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
    name VARCHAR(128) NOT NULL,
    PRIMARY KEY(idPayee)
);

CREATE TABLE DatabaseInfos(
    infoId VARCHAR(128) NOT NULL,
    infoValue VARCHAR(256),
    PRIMARY KEY(infoId)
);

CREATE TABLE AssoTransactionsScheduledTransactions(
    idTransaction INTEGER NOT NULL,
    idScheduledTransactions INTEGER NOT NULL,
    PRIMARY KEY(idTransaction, idScheduledTransactions)
);

--
-- View creation
--

