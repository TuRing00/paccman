--
-- Generated on 2014-12-05 00:09:26.426014 from MySQL Workbench using mysql2derby_plugin, version 10.
--

--
-- Tables and view deletion
--

--
-- View deletion
--

DROP TABLE ObjAccountTypes;

DROP TABLE ObjAddresses;

DROP TABLE RefCountries;

DROP TABLE ObjInfos;

DROP TABLE ObjPersons;

DROP TABLE ObjBanks;

DROP TABLE ObjAccounts;

DROP TABLE ObjTransactions;

DROP TABLE ObjPaiementModes;

DROP TABLE ObjTransactionEntries;

DROP TABLE ObjCategories;

DROP TABLE ObjCategoryGroups;

DROP TABLE ObjTransfers;

DROP TABLE ObjTransactionModels;

DROP TABLE AssoCategoriesTransactionModels;

DROP TABLE ObjScheduledTransactions;

DROP TABLE ObjPayees;

DROP TABLE DatabaseInfos;

DROP TABLE AssoTransactionsScheduledTransactions;

