/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
@Entity
@Table(name = "OBJACCOUNTS")
@NamedQueries({
    @NamedQuery(name = "Account.findAll", query = "SELECT a FROM Account a"),
    @NamedQuery(name = "Account.findByIdaccount", query = "SELECT a FROM Account a WHERE a.idaccount = :idaccount"),
    @NamedQuery(name = "Account.findByAccountname", query = "SELECT a FROM Account a WHERE a.accountname = :accountname"),
    @NamedQuery(name = "Account.findByAccountnumber", query = "SELECT a FROM Account a WHERE a.accountnumber = :accountnumber"),
    @NamedQuery(name = "Account.findByAccountnumberkey", query = "SELECT a FROM Account a WHERE a.accountnumberkey = :accountnumberkey"),
    @NamedQuery(name = "Account.findByInitialbalance", query = "SELECT a FROM Account a WHERE a.initialbalance = :initialbalance"),
    @NamedQuery(name = "Account.findByLastreconciliationdate", query = "SELECT a FROM Account a WHERE a.lastreconciliationdate = :lastreconciliationdate"),
    @NamedQuery(name = "Account.findByLastreconsiliationdate", query = "SELECT a FROM Account a WHERE a.lastreconsiliationdate = :lastreconsiliationdate"),
    @NamedQuery(name = "Account.findByReconciliationstatus", query = "SELECT a FROM Account a WHERE a.reconciliationstatus = :reconciliationstatus")})
public class Account implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDACCOUNT")
    private Integer idaccount;

    @Basic(optional = false)
    @Column(name = "ACCOUNTNAME")
    private String accountname;

    @Column(name = "ACCOUNTNUMBER")
    private String accountnumber;

    @Column(name = "ACCOUNTNUMBERKEY")
    private String accountnumberkey;

    @Basic(optional = false)
    @Column(name = "INITIALBALANCE")
    private int initialbalance;

    @Column(name = "LASTRECONCILIATIONDATE")
    @Temporal(TemporalType.DATE)
    private Date lastreconciliationdate;

    @Column(name = "LASTRECONSILIATIONDATE")
    @Temporal(TemporalType.DATE)
    private Date lastreconsiliationdate;

    @Basic(optional = false)
    @Column(name = "RECONCILIATIONSTATUS")
    private char reconciliationstatus;

    @JoinColumn(name = "IDACCOUNTHOLDER", referencedColumnName = "IDPERSON")
    @ManyToOne
    private Person person;

    @JoinColumn(name = "IDBANK", referencedColumnName = "IDBANK")
    @ManyToOne(optional = false)
    private Bank bank;

    @JoinColumn(name = "IDACCOUNTTYPE", referencedColumnName = "IDACCOUNTTYPE")
    @ManyToOne(optional = false)
    private AccountType accountType;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "account")
    private Collection<Transaction> transactionCollection;

    public Account() {
    }

    public Account(Integer idaccount) {
        this.idaccount = idaccount;
    }

    public Account(Integer idaccount, String accountname, int initialbalance, char reconciliationstatus) {
        this.idaccount = idaccount;
        this.accountname = accountname;
        this.initialbalance = initialbalance;
        this.reconciliationstatus = reconciliationstatus;
    }

    public Integer getIdaccount() {
        return idaccount;
    }

    public void setIdaccount(Integer idaccount) {
        this.idaccount = idaccount;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public String getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }

    public String getAccountnumberkey() {
        return accountnumberkey;
    }

    public void setAccountnumberkey(String accountnumberkey) {
        this.accountnumberkey = accountnumberkey;
    }

    public int getInitialbalance() {
        return initialbalance;
    }

    public void setInitialbalance(int initialbalance) {
        this.initialbalance = initialbalance;
    }

    public Date getLastreconciliationdate() {
        return lastreconciliationdate;
    }

    public void setLastreconciliationdate(Date lastreconciliationdate) {
        this.lastreconciliationdate = lastreconciliationdate;
    }

    public Date getLastreconsiliationdate() {
        return lastreconsiliationdate;
    }

    public void setLastreconsiliationdate(Date lastreconsiliationdate) {
        this.lastreconsiliationdate = lastreconsiliationdate;
    }

    public char getReconciliationstatus() {
        return reconciliationstatus;
    }

    public void setReconciliationstatus(char reconciliationstatus) {
        this.reconciliationstatus = reconciliationstatus;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public Collection<Transaction> getTransactionCollection() {
        return transactionCollection;
    }

    public void setTransactionCollection(Collection<Transaction> transactionCollection) {
        this.transactionCollection = transactionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idaccount != null ? idaccount.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Account)) {
            return false;
        }
        Account other = (Account) object;
        if ((this.idaccount == null && other.idaccount != null) || (this.idaccount != null && !this.idaccount.equals(other.idaccount))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.moonstice.paccman.data.Account[idaccount=" + idaccount + "]";
    }

}
