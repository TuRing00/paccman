/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
@Entity
@Table(name = "OBJACCOUNTTYPES")
@NamedQueries({
    @NamedQuery(name = "AccountType.findAll", query = "SELECT a FROM AccountType a"),
    @NamedQuery(name = "AccountType.findByIdaccounttype", query = "SELECT a FROM AccountType a WHERE a.idaccounttype = :idaccounttype"),
    @NamedQuery(name = "AccountType.findByAccounttypename", query = "SELECT a FROM AccountType a WHERE a.accounttypename = :accounttypename")})
public class AccountType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDACCOUNTTYPE")
    private Integer idaccounttype;

    @Basic(optional = false)
    @Column(name = "ACCOUNTTYPENAME")
    private String accounttypename;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "accountType")
    private Collection<Account> accountCollection;

    public AccountType() {
    }

    public AccountType(Integer idaccounttype) {
        this.idaccounttype = idaccounttype;
    }

    public AccountType(Integer idaccounttype, String accounttypename) {
        this.idaccounttype = idaccounttype;
        this.accounttypename = accounttypename;
    }

    public Integer getIdaccounttype() {
        return idaccounttype;
    }

    public void setIdaccounttype(Integer idaccounttype) {
        this.idaccounttype = idaccounttype;
    }

    public String getAccounttypename() {
        return accounttypename;
    }

    public void setAccounttypename(String accounttypename) {
        this.accounttypename = accounttypename;
    }

    public Collection<Account> getAccountCollection() {
        return accountCollection;
    }

    public void setAccountCollection(Collection<Account> accountCollection) {
        this.accountCollection = accountCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idaccounttype != null ? idaccounttype.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountType)) {
            return false;
        }
        AccountType other = (AccountType) object;
        if ((this.idaccounttype == null && other.idaccounttype != null) || (this.idaccounttype != null && !this.idaccounttype.equals(other.idaccounttype))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.moonstice.paccman.data.AccountType[idaccounttype=" + idaccounttype + "]";
    }

}
