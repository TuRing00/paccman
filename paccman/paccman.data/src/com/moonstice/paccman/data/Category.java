/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
@Entity
@Table(name = "OBJCATEGORIES")
@NamedQueries({
    @NamedQuery(name = "Category.findAll", query = "SELECT c FROM Category c"),
    @NamedQuery(name = "Category.findByIdcategory", query = "SELECT c FROM Category c WHERE c.idcategory = :idcategory"),
    @NamedQuery(name = "Category.findByCategoryname", query = "SELECT c FROM Category c WHERE c.categoryname = :categoryname"),
    @NamedQuery(name = "Category.findByDescription", query = "SELECT c FROM Category c WHERE c.description = :description"),
    @NamedQuery(name = "Category.findByIsincome", query = "SELECT c FROM Category c WHERE c.isincome = :isincome")})
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDCATEGORY")
    private Integer idcategory;

    @Basic(optional = false)
    @Column(name = "CATEGORYNAME")
    private String categoryname;

    @Column(name = "DESCRIPTION")
    private String description;

    @Basic(optional = false)
    @Column(name = "ISINCOME")
    private short isincome;

    @JoinTable(name = "ASSOCATEGORIESTRANSACTIONMODELS", joinColumns = {
        @JoinColumn(name = "IDCATEGORY", referencedColumnName = "IDCATEGORY")}, inverseJoinColumns = {
        @JoinColumn(name = "IDTRANSACTIONMODEL", referencedColumnName = "IDTRANSACTIONMODEL")})
    @ManyToMany
    private Collection<TransactionModel> transactionModelCollection;

    @JoinColumn(name = "IDCATEGORYGROUP", referencedColumnName = "IDCATEGORYGROUP")
    @ManyToOne(optional = false)
    private CategoryGroup categoryGroup;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "category")
    private Collection<TransactionEntry> transactionEntryCollection;

    public Category() {
    }

    public Category(Integer idcategory) {
        this.idcategory = idcategory;
    }

    public Category(Integer idcategory, String categoryname, short isincome) {
        this.idcategory = idcategory;
        this.categoryname = categoryname;
        this.isincome = isincome;
    }

    public Integer getIdcategory() {
        return idcategory;
    }

    public void setIdcategory(Integer idcategory) {
        this.idcategory = idcategory;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public short getIsincome() {
        return isincome;
    }

    public void setIsincome(short isincome) {
        this.isincome = isincome;
    }

    public Collection<TransactionModel> getTransactionModelCollection() {
        return transactionModelCollection;
    }

    public void setTransactionModelCollection(Collection<TransactionModel> transactionModelCollection) {
        this.transactionModelCollection = transactionModelCollection;
    }

    public CategoryGroup getCategoryGroup() {
        return categoryGroup;
    }

    public void setCategoryGroup(CategoryGroup categoryGroup) {
        this.categoryGroup = categoryGroup;
    }

    public Collection<TransactionEntry> getTransactionEntryCollection() {
        return transactionEntryCollection;
    }

    public void setTransactionEntryCollection(Collection<TransactionEntry> transactionEntryCollection) {
        this.transactionEntryCollection = transactionEntryCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcategory != null ? idcategory.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Category)) {
            return false;
        }
        Category other = (Category) object;
        if ((this.idcategory == null && other.idcategory != null) || (this.idcategory != null && !this.idcategory.equals(other.idcategory))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.moonstice.paccman.data.Category[idcategory=" + idcategory + "]";
    }

}
