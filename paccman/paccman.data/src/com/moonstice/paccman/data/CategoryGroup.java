/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
@Entity
@Table(name = "OBJCATEGORYGROUPS")
@NamedQueries({
    @NamedQuery(name = "CategoryGroup.findAll", query = "SELECT c FROM CategoryGroup c"),
    @NamedQuery(name = "CategoryGroup.findByIdcategorygroup", query = "SELECT c FROM CategoryGroup c WHERE c.idcategorygroup = :idcategorygroup"),
    @NamedQuery(name = "CategoryGroup.findByCategorygroupname", query = "SELECT c FROM CategoryGroup c WHERE c.categorygroupname = :categorygroupname"),
    @NamedQuery(name = "CategoryGroup.findByDescription", query = "SELECT c FROM CategoryGroup c WHERE c.description = :description")})
public class CategoryGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDCATEGORYGROUP")
    private Integer idcategorygroup;

    @Basic(optional = false)
    @Column(name = "CATEGORYGROUPNAME")
    private String categorygroupname;

    @Column(name = "DESCRIPTION")
    private String description;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoryGroup")
    private Collection<Category> categoryCollection;

    public CategoryGroup() {
    }

    public CategoryGroup(Integer idcategorygroup) {
        this.idcategorygroup = idcategorygroup;
    }

    public CategoryGroup(Integer idcategorygroup, String categorygroupname) {
        this.idcategorygroup = idcategorygroup;
        this.categorygroupname = categorygroupname;
    }

    public Integer getIdcategorygroup() {
        return idcategorygroup;
    }

    public void setIdcategorygroup(Integer idcategorygroup) {
        this.idcategorygroup = idcategorygroup;
    }

    public String getCategorygroupname() {
        return categorygroupname;
    }

    public void setCategorygroupname(String categorygroupname) {
        this.categorygroupname = categorygroupname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Category> getCategoryCollection() {
        return categoryCollection;
    }

    public void setCategoryCollection(Collection<Category> categoryCollection) {
        this.categoryCollection = categoryCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcategorygroup != null ? idcategorygroup.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoryGroup)) {
            return false;
        }
        CategoryGroup other = (CategoryGroup) object;
        if ((this.idcategorygroup == null && other.idcategorygroup != null) || (this.idcategorygroup != null && !this.idcategorygroup.equals(other.idcategorygroup))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.moonstice.paccman.data.CategoryGroup[idcategorygroup=" + idcategorygroup + "]";
    }

}
