/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
@Entity
@Table(name = "REFCOUNTRIES")
@NamedQueries({
    @NamedQuery(name = "Country.findAll", query = "SELECT c FROM Country c"),
    @NamedQuery(name = "Country.findByCodeiso3166", query = "SELECT c FROM Country c WHERE c.codeiso3166 = :codeiso3166"),
    @NamedQuery(name = "Country.findByEnglishname", query = "SELECT c FROM Country c WHERE c.englishname = :englishname")})
public class Country implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "CODEISO3166")
    private String codeiso3166;

    @Basic(optional = false)
    @Column(name = "ENGLISHNAME")
    private String englishname;

    @OneToMany(mappedBy = "country")
    private Collection<Address> addressCollection;

    public Country() {
    }

    public Country(String codeiso3166) {
        this.codeiso3166 = codeiso3166;
    }

    public Country(String codeiso3166, String englishname) {
        this.codeiso3166 = codeiso3166;
        this.englishname = englishname;
    }

    public String getCodeiso3166() {
        return codeiso3166;
    }

    public void setCodeiso3166(String codeiso3166) {
        this.codeiso3166 = codeiso3166;
    }

    public String getEnglishname() {
        return englishname;
    }

    public void setEnglishname(String englishname) {
        this.englishname = englishname;
    }

    public Collection<Address> getAddressCollection() {
        return addressCollection;
    }

    public void setAddressCollection(Collection<Address> addressCollection) {
        this.addressCollection = addressCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codeiso3166 != null ? codeiso3166.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Country)) {
            return false;
        }
        Country other = (Country) object;
        if ((this.codeiso3166 == null && other.codeiso3166 != null) || (this.codeiso3166 != null && !this.codeiso3166.equals(other.codeiso3166))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.moonstice.paccman.data.Country[codeiso3166=" + codeiso3166 + "]";
    }

}
