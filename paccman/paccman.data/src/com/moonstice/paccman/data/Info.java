/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
@Entity
@Table(name = "OBJINFOS")
@NamedQueries({
    @NamedQuery(name = "Info.findAll", query = "SELECT i FROM Info i"),
    @NamedQuery(name = "Info.findByIdinfo", query = "SELECT i FROM Info i WHERE i.idinfo = :idinfo"),
    @NamedQuery(name = "Info.findByPhone1", query = "SELECT i FROM Info i WHERE i.phone1 = :phone1"),
    @NamedQuery(name = "Info.findByPhone2", query = "SELECT i FROM Info i WHERE i.phone2 = :phone2"),
    @NamedQuery(name = "Info.findByFax", query = "SELECT i FROM Info i WHERE i.fax = :fax"),
    @NamedQuery(name = "Info.findByEmail", query = "SELECT i FROM Info i WHERE i.email = :email"),
    @NamedQuery(name = "Info.findByWeb", query = "SELECT i FROM Info i WHERE i.web = :web")})
public class Info implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDINFO")
    private Integer idinfo;

    @Column(name = "PHONE1")
    private String phone1;

    @Column(name = "PHONE2")
    private String phone2;

    @Column(name = "FAX")
    private String fax;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "WEB")
    private String web;

    @OneToMany(mappedBy = "info")
    private Collection<Person> personCollection;

    @OneToMany(mappedBy = "info")
    private Collection<Bank> bankCollection;

    public Info() {
    }

    public Info(Integer idinfo) {
        this.idinfo = idinfo;
    }

    public Integer getIdinfo() {
        return idinfo;
    }

    public void setIdinfo(Integer idinfo) {
        this.idinfo = idinfo;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public Collection<Person> getPersonCollection() {
        return personCollection;
    }

    public void setPersonCollection(Collection<Person> personCollection) {
        this.personCollection = personCollection;
    }

    public Collection<Bank> getBankCollection() {
        return bankCollection;
    }

    public void setBankCollection(Collection<Bank> bankCollection) {
        this.bankCollection = bankCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idinfo != null ? idinfo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Info)) {
            return false;
        }
        Info other = (Info) object;
        if ((this.idinfo == null && other.idinfo != null) || (this.idinfo != null && !this.idinfo.equals(other.idinfo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.moonstice.paccman.data.Info[idinfo=" + idinfo + "]";
    }

}
