/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
@Entity
@Table(name = "OBJPAYEES")
@NamedQueries({
    @NamedQuery(name = "Payee.findAll", query = "SELECT p FROM Payee p"),
    @NamedQuery(name = "Payee.findByIdpayee", query = "SELECT p FROM Payee p WHERE p.idpayee = :idpayee"),
    @NamedQuery(name = "Payee.findByPayeename", query = "SELECT p FROM Payee p WHERE p.payeename = :payeename")})
public class Payee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDPAYEE")
    private Integer idpayee;

    @Basic(optional = false)
    @Column(name = "PAYEENAME")
    private String payeename;

    @OneToMany(mappedBy = "payee")
    private Collection<Transaction> transactionCollection;

    public Payee() {
    }

    public Payee(Integer idpayee) {
        this.idpayee = idpayee;
    }

    public Payee(Integer idpayee, String payeename) {
        this.idpayee = idpayee;
        this.payeename = payeename;
    }

    public Integer getIdpayee() {
        return idpayee;
    }

    public void setIdpayee(Integer idpayee) {
        this.idpayee = idpayee;
    }

    public String getPayeename() {
        return payeename;
    }

    public void setPayeename(String payeename) {
        this.payeename = payeename;
    }

    public Collection<Transaction> getTransactionCollection() {
        return transactionCollection;
    }

    public void setTransactionCollection(Collection<Transaction> transactionCollection) {
        this.transactionCollection = transactionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpayee != null ? idpayee.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Payee)) {
            return false;
        }
        Payee other = (Payee) object;
        if ((this.idpayee == null && other.idpayee != null) || (this.idpayee != null && !this.idpayee.equals(other.idpayee))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.moonstice.paccman.data.Payee[idpayee=" + idpayee + "]";
    }

}
