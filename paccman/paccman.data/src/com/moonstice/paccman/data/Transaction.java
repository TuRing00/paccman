/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
@Entity
@Table(name = "OBJTRANSACTIONS")
@NamedQueries({
    @NamedQuery(name = "Transaction.findAll", query = "SELECT t FROM Transaction t"),
    @NamedQuery(name = "Transaction.findByIdtransaction", query = "SELECT t FROM Transaction t WHERE t.idtransaction = :idtransaction"),
    @NamedQuery(name = "Transaction.findByLabel", query = "SELECT t FROM Transaction t WHERE t.label = :label"),
    @NamedQuery(name = "Transaction.findByReconciliationstatus", query = "SELECT t FROM Transaction t WHERE t.reconciliationstatus = :reconciliationstatus"),
    @NamedQuery(name = "Transaction.findByReconciliationdate", query = "SELECT t FROM Transaction t WHERE t.reconciliationdate = :reconciliationdate"),
    @NamedQuery(name = "Transaction.findByTransactiondate", query = "SELECT t FROM Transaction t WHERE t.transactiondate = :transactiondate"),
    @NamedQuery(name = "Transaction.findByValuedate", query = "SELECT t FROM Transaction t WHERE t.valuedate = :valuedate")})
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDTRANSACTION")
    private Integer idtransaction;

    @Column(name = "LABEL")
    private String label;

    @Basic(optional = false)
    @Column(name = "RECONCILIATIONSTATUS")
    private char reconciliationstatus;

    @Column(name = "RECONCILIATIONDATE")
    @Temporal(TemporalType.DATE)
    private Date reconciliationdate;

    @Basic(optional = false)
    @Column(name = "TRANSACTIONDATE")
    @Temporal(TemporalType.DATE)
    private Date transactiondate;

    @Basic(optional = false)
    @Column(name = "VALUEDATE")
    @Temporal(TemporalType.DATE)
    private Date valuedate;

    @ManyToMany(mappedBy = "transactionCollection")
    private Collection<ScheduledTransaction> scheduledTransactionCollection;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "transaction")
    private ScheduledTransaction scheduledTransaction;

    @JoinColumn(name = "IDPAYEE", referencedColumnName = "IDPAYEE")
    @ManyToOne
    private Payee payee;

    @JoinColumn(name = "IDPAIEMENTMODE", referencedColumnName = "IDPAIEMENTMODE")
    @ManyToOne
    private PaiementMode paiementMode;

    @JoinColumn(name = "IDACCOUNT", referencedColumnName = "IDACCOUNT")
    @ManyToOne(optional = false)
    private Account account;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "transaction")
    private Collection<TransactionEntry> transactionEntryCollection;

    public Transaction() {
    }

    public Transaction(Integer idtransaction) {
        this.idtransaction = idtransaction;
    }

    public Transaction(Integer idtransaction, char reconciliationstatus, Date transactiondate, Date valuedate) {
        this.idtransaction = idtransaction;
        this.reconciliationstatus = reconciliationstatus;
        this.transactiondate = transactiondate;
        this.valuedate = valuedate;
    }

    public Integer getIdtransaction() {
        return idtransaction;
    }

    public void setIdtransaction(Integer idtransaction) {
        this.idtransaction = idtransaction;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public char getReconciliationstatus() {
        return reconciliationstatus;
    }

    public void setReconciliationstatus(char reconciliationstatus) {
        this.reconciliationstatus = reconciliationstatus;
    }

    public Date getReconciliationdate() {
        return reconciliationdate;
    }

    public void setReconciliationdate(Date reconciliationdate) {
        this.reconciliationdate = reconciliationdate;
    }

    public Date getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(Date transactiondate) {
        this.transactiondate = transactiondate;
    }

    public Date getValuedate() {
        return valuedate;
    }

    public void setValuedate(Date valuedate) {
        this.valuedate = valuedate;
    }

    public Collection<ScheduledTransaction> getScheduledTransactionCollection() {
        return scheduledTransactionCollection;
    }

    public void setScheduledTransactionCollection(Collection<ScheduledTransaction> scheduledTransactionCollection) {
        this.scheduledTransactionCollection = scheduledTransactionCollection;
    }

    public ScheduledTransaction getScheduledTransaction() {
        return scheduledTransaction;
    }

    public void setScheduledTransaction(ScheduledTransaction scheduledTransaction) {
        this.scheduledTransaction = scheduledTransaction;
    }

    public Payee getPayee() {
        return payee;
    }

    public void setPayee(Payee payee) {
        this.payee = payee;
    }

    public PaiementMode getPaiementMode() {
        return paiementMode;
    }

    public void setPaiementMode(PaiementMode paiementMode) {
        this.paiementMode = paiementMode;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Collection<TransactionEntry> getTransactionEntryCollection() {
        return transactionEntryCollection;
    }

    public void setTransactionEntryCollection(Collection<TransactionEntry> transactionEntryCollection) {
        this.transactionEntryCollection = transactionEntryCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtransaction != null ? idtransaction.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transaction)) {
            return false;
        }
        Transaction other = (Transaction) object;
        if ((this.idtransaction == null && other.idtransaction != null) || (this.idtransaction != null && !this.idtransaction.equals(other.idtransaction))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.moonstice.paccman.data.Transaction[idtransaction=" + idtransaction + "]";
    }

}
