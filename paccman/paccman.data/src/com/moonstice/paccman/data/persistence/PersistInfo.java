/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data.persistence;

import java.util.Calendar;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
public class PersistInfo {

    private String name;

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    private Calendar creationDate;

    /**
     * Get the value of creationDate
     *
     * @return the value of creationDate
     */
    public Calendar getCreationDate() {
        return creationDate;
    }

    /**
     * Set the value of creationDate
     *
     * @param creationDate new value of creationDate
     */
    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    private Calendar lastUpdateDate;

    /**
     * Get the value of lastUpdateDate
     *
     * @return the value of lastUpdateDate
     */
    public Calendar getLastUpdateDate() {
        return lastUpdateDate;
    }

    /**
     * Set the value of lastUpdateDate
     *
     * @param lastUpdateDate new value of lastUpdateDate
     */
    public void setLastUpdateDate(Calendar lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    private String modelVersion;

    /**
     * Get the value of modelVersion
     *
     * @return the value of modelVersion
     */
    public String getModelVersion() {
        return modelVersion;
    }

    /**
     * Set the value of modelVersion
     *
     * @param modelVersion new value of modelVersion
     */
    public void setModelVersion(String modelVersion) {
        this.modelVersion = modelVersion;
    }

    private Calendar lastAccessDate;

    /**
     * Get the value of lastAcessDate
     *
     * @return the value of lastAccessDate
     */
    public Calendar getLastAccessDate() {
        return lastAccessDate;
    }

    /**
     * Set the value of lastAccessDate
     *
     * @param lastAccessDate new value of lastAccessDate
     */
    public void setLastAccessDate(Calendar lastAccessDate) {
        this.lastAccessDate = lastAccessDate;
    }

}
